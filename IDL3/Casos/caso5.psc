Proceso caso5
	Definir cnotas, nota Como Real
	nota <- 0
	aprobado <- 0
	desaprobado <- 0
	caprobado <- 0
	cdesaprobado <- 0
	Escribir "¿Cuantas notas ingresara?"
	Leer cnotas
	Para x <- 1 Hasta cnotas Con Paso 1 Hacer
		Escribir "Ingrese la nota ",x
		Leer nota
		Si (nota > 13) Entonces
			aprobado <- aprobado + nota
			caprobado <- caprobado + 1
		FinSi
		Si (nota <= 13) Entonces
			desaprobado <- desaprobado + nota
			cdesaprobado <- cdesaprobado + 1
		FinSi
	Fin Para
	Escribir "Promedio de notas aprobadas es: ", aprobado / caprobado
	Escribir "Promedio de notas desaprobadas es: ", desaprobado / cdesaprobado
FinProceso
