Proceso caso2
	Definir x, n, pares, impares, positivos, negativos Como Real
	pares <- 0
	impares <- 0
	positivos <- 0
	negativos <- 0
	Para x <- 1 Hasta 10 Con Paso 1 Hacer
		Escribir "Ingrese n�mero ", x , ":"
		Leer n
		Si (n MOD 2) = 0 Entonces
			pares <- pares + 1
		SiNo
			impares <- impares + 1
		FinSi
		Si (n > 0) Entonces
			positivos <- positivos + 1
		SiNo
			negativos <- negativos + 1
		FinSi
	Fin Para
	Escribir "Total de pares: ",pares
	Escribir "Total de impares: ",impares
	Escribir "Total de positivos: ",positivos
	Escribir "Total de negativos: ",negativos
FinProceso