Proceso caso4
	Definir c, n, max, min Como Real
	max <- 0
	Escribir "Ingresa la cantidad de numeros"
	leer c
	Para x <- 1 Hasta c Con Paso 1 Hacer
		Escribir "Ingresa el n�mero ",x
		Leer n
		Si (n > max) Entonces
			max <- n
			min <- n
		SiNo
			Si (n >= max) Entonces
				max <- n
			SiNo
				Si (n < min) Entonces
					min <- n
				FinSi
			FinSi
		FinSi
	Fin Para
	Escribir "El n�mero m�ximo es: ",max
	Escribir "El n�mero m�nimo es: ",min
FinProceso
