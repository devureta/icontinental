[IContinental]
[Fundamentos de programación]

--Casos Propuestos idl1

## Ejercicio 1 ✓
1. Inicio
2. Definir radio, area Como Real
3. Escribir "Ingrese el valor del radio"
4. Leer radio
5. area = 3.1416 * (radio ^ 2)
6. Escribir "El área del circulo es ",area
7. Fin

## Ejercicio 2 ✓
1. Inicio
2. Definir altura, radio, volumen Como Real
3. Escribir "Ingrese la altura"
4. Leer altura
5. Escribir "Ingrese el radio"
6. Leer radio
7. volumen = (altura * 3.1416 * (radio ^ 2)) / 3
8. Escribir "El volumen del cono es ",volumen	
9. Fin

## Ejercicio 3 ✓
1. Inicio
2. Definir venta1, venta2, venta3, comision, sueldob, sueldot Como Real
3. Escribir "Ingrese el monto de la primera venta"
4. leer venta1
5. Escribir "Ingrese el monto de la segunda venta"
6. Leer venta2
7. Escribir "Ingrese el monto de la tercera venta"
8. Leer venta3
9. Escribir "Ingrese su sueldo base"
10. Leer sueldob
11. comision = (venta1 + venta2 + venta3) * 0.14
12. sueldot = sueldob + comision
13. Escribir "El sueldo base es ",sueldob
14. Escribir "La comision del mes es ",comision
15. Escribir "El sueldo total del mes es ",sueldot	
16. Fin

# Ejercicio 4 ✓
1. Inicio
2. Definir hh, mm, ss, tiempo, tsegundos, trestante Como Real
3. Escribir "Ingresa la hora de salida"
4. Leer hh
5. Escribir "Ingresa los minutos de salida"
6. Leer mm
7. Escribir "Ingresa los segundos de salida"
8. Leer ss
9. Escribir "Ingresa la duracion del viaje en segundos"
10. Leer tiempo
	<!-- Convertimos la hora dada por el usuario a segundos y le sumamos el tiempo del trayecto -->
11. tsegundos = (hh * 3600) + (mm * 60 ) + ss + tiempo
	<!-- Volvemos a convertir, ahora el total de segundos al formato hora. -->
12. hh = trunc(tsegundos / 3600)
13. trestante = tsegundos % 3600
14. mm = trunc(trestante / 60)
15. ss = trunc(trestante % 60)
16. Escribir "La hora de llegada a la ciudad B será de: ",hh ," horas,",mm ," minutos, ",ss ," segundos"
17. Fin

## Ejercicio 5 ✓
1. Inicio
2. Definir años, meses, semanas, dias, horas Como Real
3. Escribir "Ingrese sus años"
4. Leer años
5. meses = años * 12
6. semanas = meses * 52.1 <!-- dato brindado por google -->
7. dias = semanas * 7
8. horas = dias * 24
9. Escribir "Segun sus años usted ha vivido ",meses, " meses, ",semanas, " semanas, ",dias, " días y ",horas, " horas."
10. Fin

## Ejercicio 6 ✓
1. Inicio
2. Definir precio, horas, total Como Real
3. Escribir "Ingrese el precio"
4. Leer precio
5. Escribir "Ingrese las horas"
6. Leer horas
7. total = precio * redon(horas)
8. Escribir "El monto a pagar por ",horas, " horas es ",total	
9. Fin

## Ejercicio 7 ✓
1. Inicio
2. Definir correcta, incorrecta, blanco, notafinal, notamax, notasobre20 Como Real
3. Escribir "Ingrese el número de respuestas correctas"
4. Leer correcta
5. Escribir "Ingrese el número de respuestas incorrectas"
6. Leer incorrecta
7. Escribir "Ingrese el núnero de respuestas en blanco"
8. Leer blanco
9. notamax <- (correcta + incorrecta + blanco) * 5
10. notafinal <- (correcta * 5) + (incorrecta * (-1))
11. notasobre20 <- (notafinal * 20) / notamax
12. Escribir "La nota final del alumno es ",notafinal, " con nota sobre 20 de ",notasobre20
13. Fin

## Ejercicio 8 ✓
1. Inicio
2. Definir v, i, r, p Como Real
3. Escribir "Ingrese el valor de la corriente"
4. Leer i
6. r = 4
7. v = i * r
8. p = v * i
9. Escribir "El valor de la corriente es de ",i
10. Escribir "EL valor de la resistencia es de ",r
10. Escribir "El voltaje es de ",v
11. Escribir "La potencia eléctrica es de ",p	
12. Fin

## Ejercicio 9 ✓
1. Inicio
2. Definir distancia, gas, gaso, petro, ahorro1, ahorro2 Como Real
3. Escribir "Ingrese la distancia que desea recorrer"
4. Leer distancia
5. gas = distancia * 2.3
6. gaso = distancia * 7.5
7. petro = distancia * 3.4
8. Escribir "En ",distancia "km recorridos se ha gastado de dinero: "
9. Escribir "Gas: ",gas
10. Escribir "Gasolina: ",gaso
11. Escribir "Petróleo: ",petro
12. ahorro1 = gaso - gas
13. ahorro2 = gaso - petro
14. Escribir "El ahorro frente al uso de gasolina es de:"
15. Escribir "Gas ",ahorro1
16. Escribir "Petróleo ",ahorro2
17. Fin

## Ejercicio 10 ✓
1. Inicio
2. Definir b200, b100, b50, b20, b10, b1, retiro Como Entero
3. Escribir "Ingrese cantidad a retirar"
4. Leer retiro
5. b200 = (retiro DIV 200)
6. retiro = retiro - b200 * 200
7. b100 = (retiro DIV 100)
8. retiro = retiro - b100 * 100
9. b50 = (retiro DIV 50)
10. retiro = retiro - b50 * 50
11. b20 = (retiro DIV 20)
12. retiro = retiro - b20 * 20
13. b10 = (retiro DIV 10)
14. retiro = retiro - b10 * 10
15. b1 = (retiro DIV 1)
16. retiro = retiro - b1 * 1
17. Escribir "Billetes de 200 ",b200, retiro
18. Escribir "Billetes de 100 ",b100, retiro
19. Escribir "Billetes de 50 ",b50, retiro
20. Escribir "Billetes de 20 ",b20, retiro
21. Escribir "Billetes de 10 ",b10, retiro
22. Escribir "Billetes de 1 ",b1, retiro
23. Fin