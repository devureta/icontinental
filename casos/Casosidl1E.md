[IContinental]
[Fundamentos de programación]

# Ejercicios: Casos propuestos  

* Realizar un programa que requiere obtener el área de un círculo.  

* Realizar un programa que permita calcular el volumen de un cono.  

* Un vendedor recibe su sueldo base más 14% extra por comisión de sus ventas; el vendedor desea saber cuánto dinero recibirá por concepto de comisiones por las tres ventas que realiza en el mes y el total que recibirá en el mes tomando en cuenta sueldo base y comisiones.  

* Un ciclista parte de una ciudad A a las HH horas, MM minutos y SS segundos. El tiempo de viaje hasta llegar a otra ciudad B es de T segundos. Escribir un proga,a que determine la hora de llegada a la ciudad B.  

* Realice un programa que representen el algoritmo donde se ingrese la edad de una persona para determinar aproximadamente cuántos meses, semanas, días y horas ha vivido una persona.  

* Un estacionamiento requiere determinar el cobro que debe aplicar a las personas que lo utilizan. Considere que el cobro es con base en las horas que lo disponen y que las fracciones de hora se toman como completas y realice un programa que permita determinar el cobro.  

* Escribir un programa para calcular la nota final de un estudiante, considerando que: por cada respuesta correcta 5 puntos, por una incorrecta -1 y por respuestas en blanco 0. Imprime el resultado obtenido por el estudiante (Valide el ingreso de 20 preguntas).  

* Se desea calcular la potencia eléctrica de circuito de la figura. Realice el programa para resolver el problema. Considere que:  ```(P = V * I) y (V = R * I)```.  

* Hallar la cantidad de dinero al gastar con tres diferentes tipos de combustible (Gas: 2.3/km, gasolina 7.5/km, petróleo 3.4/km) al recorrer la misma distancia que ingresara el usuario, también calcular el porcentaje de ahorro referente al costo de la gasolina.  

* Se necesita saber la cantidad de dinero que un cajero automático debe proporcionar en diferentes denominaciones, teniendo en cuenta que el usuario debe ingresar la cantidad a retirar y el cajero tiene disponible las siguientes denominaciones de dinero: 200, 100, 50, 20, 10, 1. Se debe buscar entregar la menor cantidad de dinero por parte del cajero.  

---

## Recursos

Lista de recursos que me ayudaron a comprender y resolver estos ejercicios.  

* [Función TRUNC](https://docs.aws.amazon.com/es_es/redshift/latest/dg/r_TRUNC.html)  
* [Función Redondear](https://acortar.link/RfaYRr)
* [Convertir una nota sobre 20](https://youtu.be/7lhghoS-eAU)