[IContinental]
[Fundamentos de programación]

--Casos Propuestos idl2

## Ejercicio 1 ✓
1. Inicio
2. Definir edad como Entero
3. Escribir "Ingrese su edad"
4. Leer edad
5. Si (edad >= 18) Entonces
6. 	Escribir "Es usted mayor de edad"
7. SiNo
8. 	Escribir "Es usted menor de edad"
9. FinSi
10. Fin

## Ejercicio 2 ✓
1. Inicio
2. Definir n1, n2 como Real
3. Escribir "Ingrese el primer número"
4. Leer n1
5. Escribir "Ingrese el segundo número"
6. Leer n2
7. Si (n2 == 0) Entonces
8. 	Escribir "Hubo un error, el divisor no puede ser 0"
9. SiNo
10. Escribir "El resultado de la division es: "n1/n2
11. FinSi
12. Fin

## Ejercicio 3 ✓
1. Inicio
2. Definir sueldo como Real
3. Escribir "Ingrese su sueldo"
4. Leer sueldo
5. Si (sueldo > 3000) Entonces
6. 	Escribir ('Debe abonar impuestos')
7. SiNo
8.	Escribir ('No debe abonar impuestos')
9. FinSi
10. Fin

# Ejercicio 4 ✓
1. Inicio
2. Definir cantidad, total Como Real
3. Escribir "Ingrese la cantidad de llantas a comprar"
4. Leer cantidad
5. Si (cantidad >= 5) Entonces
6. 	total = cantidad * 120
7. SiNo
8. 	total = cantidad * 150
9. FinSi
10. Escribir "El total a pagar es S/.",total
11. Fin

## Ejercicio 5 ✓
1. Inicio
2. Definir edad, pulsaciones Como Real
3. Definir sexo Como Caracter
4. Escribir "Ingrese su sexo"
5. Leer sexo
6. Escribir "Ingrese su edad"
7. Leer edad
8. Si (sexo = "femenino") Entonces
9. 	pulsaciones = (220 - edad) / 10
10. SiNo
11. Si (sexo = "masculino") Entonces
12. pulsaciones = (210 - edad) / 10
13. FinSi
14. FinSi
15. Escribir "Sus pulsaciones son ",pulsaciones
16. Fin

## Ejercicio 6 ✓
1. Inicio
2. Definir edad, ingresos Como Real
3. Escribir "Ingrese su edad"
4. Leer edad
5. Escribir "Ingrese sus ingresos mensuales"
6. Leer ingresos
7. Si (edad < 16) Entonces
8. 	Escribir "No puede tributar"
9. SiNo
10. Si (ingresos >= 1000) Entonces
11. Escribir "Si puede tributar"
12. SiNo
13. Escribir "No puede tributar"
14. FinSi
15. FinSi
16. Fin

## Ejercicio 7 ✓
1. Inicio
2. Definir cursos, promedio, colegiatura, descuento, total Como Real
3. Escribir "Cantidad de cursos"
4. Leer cursos
5. Escribir "Ingrese su promedio"
6. Leer promedio
7. colegiatura = cursos * 150
8. descuento = (cursos * 150) * .30
9. Si (promedio >= 17) Entonces
10. total = colegiatura - descuento
11. SiNo
12. total = colegiatura		
13. FinSi
14. Escribir "El precio de la colegiatura completa es: ",colegiatura
15. Escribir "Con el descuento usted debe pagar: ",total
16. Fin

## Ejercicio 8 ✓
1. Inicio
2. Definir cantidad, descuento, total Como Reales
3. Escribir "Ingrese la cantidad de computadoras"
4. Leer cantidad
5. total = cantidad * 2500
6. Si (cantidad < 5) Entonces
7. descuento = total * .90
8. SiNo
9. Si (cantidad >= 5) Entonces
10. descuento = total * .80
11. Si (cantidad >= 10)
12. descuento = total * .60
13. FinSi
14. FinSi
15. FinSi
16. Escribir "el precio es de S/.",descuento
17. Fin

## Ejercicio 9 ✓
1. Inicio
2. Definir edad, precio Como Real
3. Escribir "Ingrese su edad"
4. Leer edad
5. Si (edad < 4) Entonces
6. precio = 0
7. SiNo
8. Si (edad <= 17) Entonces
9. precio = 5
10. SiNo
11. Si (edad >= 18) Entonces
12. precio = 10
13. FinSi
14. FinSi
15. FinSi
16. Escribir "El precio de la entrada es de S/.",precio
17. Fin

## Ejercicio 10 ✓