[IContinental]
[Fundamentos de programación]

# Ejercicios: Casos propuestos  

* Escribir un programa que pregunte al usuario su edad y muestre por pantalla si es mayor de edad o no.  

* Escribir un programa que pida al usuario dos números y muestre por pantalla su división. Si el divisor es cero el programa debe mostrar un error.  

* Ingresar el sueldo de una persona, si supera los 3000 pesos mostrar un mensaje en pantalla indicando que debe abonar impuestos.  

* Calcular el total que una persona debe pagar en una llantera, si el precio de cada llanta es de S/150 si se compran menos de 5 llantas y de S/120 si se compran 5 o más.  
* Calcular el número de pulsaciones que debe tener una persona por cada 10 segundos de ejercicio aeróbico; la fórmula que se aplica cuando el sexo es femenino es: num_pulsaciones = (220 − edad)/10 y si el sexo es masculino: num_pulsaciones = (210 − edad)/10.  

* Para tributar un determinado impuesto se debe ser mayor de 16 años y tener unos ingresos iguales o superiores a 1000 € mensuales. Escribir un programa que pregunte al usuario su edad y sus ingresos mensuales y muestre por pantalla si el usuario tiene que tributar o no.  

* En una escuela la colegiatura de los alumnos se determina según el número de materias que cursan(Solicitar al usuario). El costo de todas las materias es el mismo S/150. Se ha establecido un programa para estimular a los alumnos, el cual consiste en lo siguiente: si el promedio obtenido por un alumno en el último periodo es mayor o igual que 17, se le hará un descuento del 30% sobre la colegiatura; si el promedio obtenido es menor que 17 deberá pagar la colegiatura completa. Obtener cuanto debe pagar un alumno.  

* En una fábrica de computadoras se planea ofrecer a los clientes un descuento que dependerá del número de computadoras que compre. Si las computadoras son menos de cinco se les dará un 10% de descuento sobre el total de la compra; si el número de computadoras es mayor o igual a cinco pero menos de diez se le otorga un 20% de descuento; y si son 10 o más se les da un 40% de descuento. El precio de cada computadora es de S/2,500.  

* Escribir un programa para una empresa que tiene salas de juegos para todas las edades y quiere calcular de forma automática el precio que debe cobrar a sus clientes por entrar. El programa debe preguntar al usuario la edad del cliente y mostrar el precio de la entrada. Si el cliente es menor de 4 años puede entrar gratis, si tiene entre 4 y 18 años debe pagar S/5 y si es mayor de 18 años, S/10.  

* Elabore un programa que lea la hora y muestre por pantalla la hora un segundo después ejemplo:
1:20:21 debe mostrar 1:20:22
1:59:59 debe mostrar 2:00:00

---

## Recursos

Lista de recursos que me ayudaron a comprender y resolver estos ejercicios.  

* [Manual de PHP](https://www.php.net/manual/es/getting-started.php)  
* [Estructura selectiva doble | if else](https://youtu.be/589EjXAVsuU)