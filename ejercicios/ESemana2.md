[IContinental]
[Fundamentos de programación]

--Ejercicios Resueltos Semana 2
--25/04/2022

## Ejercicios 3
1. Inicio
2. Definir ba, al, area tipo Decimal
3. Leer ba
4. Leer al
5. area = ba * al
6. Escribir area
7. Fin

## Ejercicio 4
1. Inicio
2. Definir n, vfuncion tipo Decimal
3. Leer n
4. vfuncion = 3(n) + 2
5. Escribir (n,vfuncion)
6. Fin

## Ejercicio 5
1. Inicio
2. Definir a, b, c, s, area
3. Leer a
4. Leer b
5. Leer c
6. s = a + b + c
7. area = (s * (s - a) * (s - b) - (s - c)) ^ 0.5
8. Escribir area
9. Fin

## Ejercicio 6
1. Inicio
2. Definir c1, a1, c2, a2, c3, a3, idl1, idl2, idl3, promedio
3. Leer c1
4. Leer a1
5. idl1 = (c1 * 0.60) + (a1 * 0.40)
6. Leer c2
7. Leer a2
8. idl2 = (c2 * 0.60) + (a2 * 0.40)
9. Leer c3
10. Leer a3
11. idl3 = (c3 * 0.60) * (a3 * 0.40)
12. promedio = (idl1 * 0.25) + (idl2 * 0.25) + (idl3 * 0.50)
13. Escribir promedio
14. Fin

## Ejercicio 7
1. Inicio
2. Definir al, an, la, vol
3. Leer al
4. Leer an
5. Leer la
6. vol = al * an * la
7. Escribir vol
8. Fin

## Ejercicio 8
1. Inicio
2. Definir arista, area
3. Leer arista
4. area = (3 ^ 0.5) * (arista * arista)
5. Escribir area
6. Fin

## Ejercicio 9
1. Inicio
2. Definir v, h, m
3. Leer v
4. h = v DIV 60
5. m = v MOD 60  /*(v / 60 - h) * 60*/
6. Escribir h, m
7. Fin

## Ejercicio 10
1. Inicio
2. Definir a, b, c, x1, x2
3. Leer a
4. Leer b
5. Leer c
6. x1 = ((-1 * b) + (b ^ 2 - 4 * a * c) ^ 0.5) / (2 * a)
7. x2 = ((-1 * b) - (b ^ 2 - 4 * a * c) ^ 0.5) / (2 * a)
8. Escribir x1
9. Escribir x2
10. Fin

## Ejercicio 11
1. Inicio
2. Definr montI, i, a, montF
3. Leer montI
4. Leer i
5. Leer a
6. montF = montI * ((1 + i / 100) ** a)
7. Escribir montF
8. Fin

## Ejercicio 12
1. Inicio
2, Definir f, c
3. Leer f
4. c = (f - 32) * 5 / 9
5. Escribir c
6. Fin

## Ejercicio 13
1. Inicio
2. Definir p1, p2, p3, igv, sub, tot
3. Leer p1
4. Leer p2
5. Leer p3
6. Leer igv
7. sub = p1 + p2 + p3 / 1.18
8. igv = p1 + p2 + p3 - sub
9. tot = p1 + p2 + p3
10. Escribir sub + igv = tot
11. Fin

## Ejercicio 14
1. Inicio
2. Definir n1, n2, n3, promedio
3. Leer n1
4. Leer n2
5. Leer n3
6. promedio = (n1 * 0.25) + (n2 * 0.25) + (n3 * 0.50)
7. Escribir promedio
8. Fin