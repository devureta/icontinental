[IContinental]
[Fundamentos de programación]

# Ejercicios idl2

* Pedir un valor numérico e indicar si es par o no es par.  

* Escribir un programa que almacene la cadena de caracteres contraseña en una variable, pregunte al usuario por la contraseña e imprima por pantalla si la contraseña introducida por el usuario coincide con la guardada en la variable sin tener en cuenta mayúsculas y minúsculas.  

* Determinar si un alumno aprueba a reprueba un curso, sabiendo que aprobara si su promedio de tres calificaciones es mayor o igual a 13; reprueba en caso contrario.  

* En un almacén se descuenta 20% del precio al cliente si el valor a pagarse es mayor a S/200. Dado un valor de precio, muestre lo que debe pagar el cliente.  

* Leer dos números y los imprima en forma ascendente.  

* Un hombre desea saber si invertir o no invertir su dinero. El decidirá invertir siempre que el total obtenido de la inversión excedan a S/7000, y en ese caso que así sea, desea saber cuánto dinero tendrá finalmente en su cuenta. ```(total = (cap * int) + cap)```  

* Por cierrapuertas en un almacén se rebaja 10% del precio al cliente si compra más de 20 artículos y 5% si compra hasta 20 artículos. Dado el precio unitario de un artículo y la cantidad adquirida, muestre lo que debe pagar el cliente.  

* Algoritmo que lea tres números diferentes e imprima en pantalla los valores máximo y mínimo.  

* ¿Dada la duración en minutos de una llamada calcular el costo, considerando? Hasta tres minutos el costo es 0.10 Por encima de tres minutos es 0.10 más por cada minuto transcurrido luego de los 3 primeros minutos.  

* Un obrero necesita calcular su salario semanal, el cual se obtiene de la siguiente manera: Si trabaja 40 horas o menos se le paga un salario de S/16 por hora, si trabaja más de 40 horas se le paga un salario de S/20 por cada una de las primeras 40 horas y un salario de S/20 por cada hora extra.  

* Una persona enferma, que pesa 70 kg, se encuentra en reposo y desea saber cuántas calorías consume su cuerpo durante todo el tiempo que realice una misma actividad(minutos). Las actividades que tiene permitido realizar son únicamente dormir o estar sentado en reposo. Los datos que tiene son que estando dormido consume 1.08 calorías por minuto y estando sentado en reposo consume 1.66 calorías por minuto.  

* Leer 2 números; si son iguales que los multiplique, si el primero es mayor que el segundo que los reste y si no que los sume.  

* Determinar el estado de un estudiante. Sabiendo que si su nota final el de 0-9 está desaprobado, de 11-12 está en proceso de recuperación y si tiene de 13-20 está aprobado.  

* Calcular la utilidad que un trabajador recibe en el reparto anual de utilidades si este se le asigna como un porcentaje de su salario mensual que depende de su antigüedad en la empresa de acuerdo con la siguiente tabla: 

| Tiempo        					| Utilidad 			|
|:----								|:-----------------:| 
| Menos de 1 año      				| 5% del salario 	|
| 1 año o mas y menos de 2 años 	| 7% del salario	|
| 2 años o mas y menos de 5 años	| 10% del salario 	|
| 5 años o mas y menos de 10 años	| 15% del salario 	|
| 10 años o mas 					| 20% del salario 	|

* En una tienda de descuento se efectúa una promoción en la cual se hace un descuento sobre el valor de la compra total según el color de la bolita que el cliente saque al pagar en caja. Si la bolita es de color blanco no se le hará descuento alguno, si es verde se le hará un 10% de descuento, si es amarilla un 25%, si es azul un 50% y si es roja un 100%. Determinar la cantidad final que el cliente deberá pagar por su compra se sabe que solo hay bolitas de los colores mencionados.